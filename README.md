As an idea agency, we shape concepts into mastered narratives for our clients. As craftsmen, we share our own interpretations of the world through film. With each project — regardless of scope — we oversee every detail from production to execution to promotion and distribution.

Address: 541 NW 1st Ave, Fort Lauderdale, Florida 33301, USA

Phone: 954-357-3934

Website: https://c-istudios.com
